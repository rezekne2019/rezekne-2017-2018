Lekciju saraksts 
===============
Projekts izstrādāts studiju kursa "Lielo projektu izstrādes principi uz .NET pamata" ietvarā. Rīks lekciju sarakstu izveidei priekš Rēzeknes Tehnoloģiju akadēmijas.

Par projektu
--------------
Projekts izstrādāts izmantojot **C#** un **Entity Framework**.

###Funkcijas
* Pievienot/labot/dzēst docētājus
* Pievienot/labot/dzēst nodarbības
* Pievienot/labot/dzēst grupas
* Pievienot/labot/dzēst lekciju kursus

###Kas nepieciešams, lai palaistu projektu
+ *Microsoft Visual Studio 2015* vai jaunāks
+ *Microsoft SQL Server 2016* vai jaunāks
+ Izolenta un banāns, ja esi Makgaivers

###Kā palaist projektu
+ Atvērt datni ar .sln paplašinājumu izmantojot *Visual Studio 2015*  vai jaunāku versiju
+ Izveidot savienojumu ar datubāzi
+ ``` Project -> Add New Data Source.. -> Database -> Dataset -> New Connection -> Microsoft SQL Server Database File```
+ Pievienot datubāzes datni, kas atrodas iekš ```..\LekcijuSaraksts\LekcijuSaraksts\LekcijuSaraksts.mdf ``` un nospiest OK
+ Nospiest *Finish*
+ Lai palaist projektu jānospiež poga *Start*




Komanda 
-------------
| Vārds  |   |
|----------|-------------|
| Artis Teilāns |  *Kursa vaditājs* | 
| Edgars |      | 
| Kaspars |     |
| Iļja |     |
| Valdis |     |
| Renāte |     |
| Mārīte |     |
| Artūrs |     |
| Uģis |     |

