
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, 2012 and Azure
-- --------------------------------------------------
-- Date Created: 12/06/2017 21:20:08
-- Generated from EDMX file: C:\Users\Edgar\Desktop\git\rezekne-2017-2018\LekcijuSaraksts\SarakstaEntites\SarakstaEntites.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[FK_LaiksNodarbiba]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[NodarbibaSet] DROP CONSTRAINT [FK_LaiksNodarbiba];
GO
IF OBJECT_ID(N'[dbo].[FK_TelpaNodarbiba]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[NodarbibaSet] DROP CONSTRAINT [FK_TelpaNodarbiba];
GO
IF OBJECT_ID(N'[dbo].[FK_DocetajsNodarbiba]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[NodarbibaSet] DROP CONSTRAINT [FK_DocetajsNodarbiba];
GO
IF OBJECT_ID(N'[dbo].[FK_LekcijuKurssNodarbiba]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[NodarbibaSet] DROP CONSTRAINT [FK_LekcijuKurssNodarbiba];
GO
IF OBJECT_ID(N'[dbo].[FK_NodarbibaGrupaNodarbiba]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[GrupaNodarbibaSet] DROP CONSTRAINT [FK_NodarbibaGrupaNodarbiba];
GO
IF OBJECT_ID(N'[dbo].[FK_GrupaGrupaNodarbiba]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[GrupaNodarbibaSet] DROP CONSTRAINT [FK_GrupaGrupaNodarbiba];
GO
IF OBJECT_ID(N'[dbo].[FK_StudijuProgrammaGrupa]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[GrupaSet] DROP CONSTRAINT [FK_StudijuProgrammaGrupa];
GO
IF OBJECT_ID(N'[dbo].[FK_DienaNodarbiba]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[NodarbibaSet] DROP CONSTRAINT [FK_DienaNodarbiba];
GO

-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[DocetajsSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[DocetajsSet];
GO
IF OBJECT_ID(N'[dbo].[LaiksSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[LaiksSet];
GO
IF OBJECT_ID(N'[dbo].[NodarbibaSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[NodarbibaSet];
GO
IF OBJECT_ID(N'[dbo].[LekcijuKurssSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[LekcijuKurssSet];
GO
IF OBJECT_ID(N'[dbo].[GrupaNodarbibaSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GrupaNodarbibaSet];
GO
IF OBJECT_ID(N'[dbo].[TelpaSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[TelpaSet];
GO
IF OBJECT_ID(N'[dbo].[GrupaSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GrupaSet];
GO
IF OBJECT_ID(N'[dbo].[StudijuProgrammaSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[StudijuProgrammaSet];
GO
IF OBJECT_ID(N'[dbo].[DienaSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[DienaSet];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'DocetajsSet'
CREATE TABLE [dbo].[DocetajsSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Vards] nvarchar(max)  NOT NULL,
    [Uzvards] nvarchar(max)  NOT NULL,
    [Akademiskais_grads] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'LaiksSet'
CREATE TABLE [dbo].[LaiksSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Nodarbibas_laiks] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'NodarbibaSet'
CREATE TABLE [dbo].[NodarbibaSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Laiks_Id] int  NOT NULL,
    [Telpa_Id] int  NOT NULL,
    [Docetajs_Id] int  NOT NULL,
    [LekcijuKurss_Id] int  NOT NULL,
    [Diena_Id] int  NOT NULL
);
GO

-- Creating table 'LekcijuKurssSet'
CREATE TABLE [dbo].[LekcijuKurssSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Nosaukums] nvarchar(max)  NOT NULL,
    [Sifrs] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'GrupaNodarbibaSet'
CREATE TABLE [dbo].[GrupaNodarbibaSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Nodarbiba_Id] int  NOT NULL,
    [Grupa_Id] int  NOT NULL
);
GO

-- Creating table 'TelpaSet'
CREATE TABLE [dbo].[TelpaSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Telpas_nr] nvarchar(max)  NOT NULL,
    [Telpas_eka] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'GrupaSet'
CREATE TABLE [dbo].[GrupaSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Semestris] nvarchar(max)  NOT NULL,
    [StudijuProgramma_Id] int  NOT NULL
);
GO

-- Creating table 'StudijuProgrammaSet'
CREATE TABLE [dbo].[StudijuProgrammaSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Nosaukums] nvarchar(max)  NOT NULL,
    [Sifrs] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'DienaSet'
CREATE TABLE [dbo].[DienaSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Nosaukums] nvarchar(max)  NOT NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [Id] in table 'DocetajsSet'
ALTER TABLE [dbo].[DocetajsSet]
ADD CONSTRAINT [PK_DocetajsSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'LaiksSet'
ALTER TABLE [dbo].[LaiksSet]
ADD CONSTRAINT [PK_LaiksSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'NodarbibaSet'
ALTER TABLE [dbo].[NodarbibaSet]
ADD CONSTRAINT [PK_NodarbibaSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'LekcijuKurssSet'
ALTER TABLE [dbo].[LekcijuKurssSet]
ADD CONSTRAINT [PK_LekcijuKurssSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'GrupaNodarbibaSet'
ALTER TABLE [dbo].[GrupaNodarbibaSet]
ADD CONSTRAINT [PK_GrupaNodarbibaSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'TelpaSet'
ALTER TABLE [dbo].[TelpaSet]
ADD CONSTRAINT [PK_TelpaSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'GrupaSet'
ALTER TABLE [dbo].[GrupaSet]
ADD CONSTRAINT [PK_GrupaSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'StudijuProgrammaSet'
ALTER TABLE [dbo].[StudijuProgrammaSet]
ADD CONSTRAINT [PK_StudijuProgrammaSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'DienaSet'
ALTER TABLE [dbo].[DienaSet]
ADD CONSTRAINT [PK_DienaSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- Creating foreign key on [Laiks_Id] in table 'NodarbibaSet'
ALTER TABLE [dbo].[NodarbibaSet]
ADD CONSTRAINT [FK_LaiksNodarbiba]
    FOREIGN KEY ([Laiks_Id])
    REFERENCES [dbo].[LaiksSet]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_LaiksNodarbiba'
CREATE INDEX [IX_FK_LaiksNodarbiba]
ON [dbo].[NodarbibaSet]
    ([Laiks_Id]);
GO

-- Creating foreign key on [Telpa_Id] in table 'NodarbibaSet'
ALTER TABLE [dbo].[NodarbibaSet]
ADD CONSTRAINT [FK_TelpaNodarbiba]
    FOREIGN KEY ([Telpa_Id])
    REFERENCES [dbo].[TelpaSet]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_TelpaNodarbiba'
CREATE INDEX [IX_FK_TelpaNodarbiba]
ON [dbo].[NodarbibaSet]
    ([Telpa_Id]);
GO

-- Creating foreign key on [Docetajs_Id] in table 'NodarbibaSet'
ALTER TABLE [dbo].[NodarbibaSet]
ADD CONSTRAINT [FK_DocetajsNodarbiba]
    FOREIGN KEY ([Docetajs_Id])
    REFERENCES [dbo].[DocetajsSet]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_DocetajsNodarbiba'
CREATE INDEX [IX_FK_DocetajsNodarbiba]
ON [dbo].[NodarbibaSet]
    ([Docetajs_Id]);
GO

-- Creating foreign key on [LekcijuKurss_Id] in table 'NodarbibaSet'
ALTER TABLE [dbo].[NodarbibaSet]
ADD CONSTRAINT [FK_LekcijuKurssNodarbiba]
    FOREIGN KEY ([LekcijuKurss_Id])
    REFERENCES [dbo].[LekcijuKurssSet]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_LekcijuKurssNodarbiba'
CREATE INDEX [IX_FK_LekcijuKurssNodarbiba]
ON [dbo].[NodarbibaSet]
    ([LekcijuKurss_Id]);
GO

-- Creating foreign key on [Nodarbiba_Id] in table 'GrupaNodarbibaSet'
ALTER TABLE [dbo].[GrupaNodarbibaSet]
ADD CONSTRAINT [FK_NodarbibaGrupaNodarbiba]
    FOREIGN KEY ([Nodarbiba_Id])
    REFERENCES [dbo].[NodarbibaSet]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_NodarbibaGrupaNodarbiba'
CREATE INDEX [IX_FK_NodarbibaGrupaNodarbiba]
ON [dbo].[GrupaNodarbibaSet]
    ([Nodarbiba_Id]);
GO

-- Creating foreign key on [Grupa_Id] in table 'GrupaNodarbibaSet'
ALTER TABLE [dbo].[GrupaNodarbibaSet]
ADD CONSTRAINT [FK_GrupaGrupaNodarbiba]
    FOREIGN KEY ([Grupa_Id])
    REFERENCES [dbo].[GrupaSet]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_GrupaGrupaNodarbiba'
CREATE INDEX [IX_FK_GrupaGrupaNodarbiba]
ON [dbo].[GrupaNodarbibaSet]
    ([Grupa_Id]);
GO

-- Creating foreign key on [StudijuProgramma_Id] in table 'GrupaSet'
ALTER TABLE [dbo].[GrupaSet]
ADD CONSTRAINT [FK_StudijuProgrammaGrupa]
    FOREIGN KEY ([StudijuProgramma_Id])
    REFERENCES [dbo].[StudijuProgrammaSet]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_StudijuProgrammaGrupa'
CREATE INDEX [IX_FK_StudijuProgrammaGrupa]
ON [dbo].[GrupaSet]
    ([StudijuProgramma_Id]);
GO

-- Creating foreign key on [Diena_Id] in table 'NodarbibaSet'
ALTER TABLE [dbo].[NodarbibaSet]
ADD CONSTRAINT [FK_DienaNodarbiba]
    FOREIGN KEY ([Diena_Id])
    REFERENCES [dbo].[DienaSet]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_DienaNodarbiba'
CREATE INDEX [IX_FK_DienaNodarbiba]
ON [dbo].[NodarbibaSet]
    ([Diena_Id]);
GO

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------