﻿using System;
using System.Windows.Forms;
using SarakstaEntites;
using System.Linq;

namespace LekcijuSaraksts
{
     public partial class StudijuProgrammasDialogs : Form
     {
          private readonly SarakstaEntitesContainer database;
          private readonly StudijuProgramma studijuProgramma;
          public StudijuProgrammasDialogs(SarakstaEntitesContainer database, StudijuProgramma studijuProgramma = null)
          {
               this.database = database;
               this.studijuProgramma = studijuProgramma;
               InitializeComponent();

               if (studijuProgramma == null)
                    this.Text = "Pievienot studiju programmu";
               else
               {
                    Text = "Rediģēt studiju programmu";
                    nameTextBox.Text = studijuProgramma.Nosaukums;
                    sifrsTextBox.Text = studijuProgramma.Sifrs;
               }
          }

          private void saveBtn_Click(object sender, EventArgs e)
          {

               if (string.IsNullOrWhiteSpace(nameTextBox.Text)
                || string.IsNullOrWhiteSpace(sifrsTextBox.Text))
               {
                    MessageBox.Show("Lūdzu aizpildiet visus laukus!");
                    return;
               }

               if (studijuProgramma == null)
               {

                if (database.StudijuProgrammaSet.Any(x => x.Nosaukums == nameTextBox.Text || x.Sifrs == sifrsTextBox.Text))
                {
                    MessageBox.Show("Ieraksts jau eksistē!",
                        "Nevar pievienot!",
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Warning);
                    return;
                }

                var _studijuProgramma = new StudijuProgramma();
                    _studijuProgramma.Nosaukums = nameTextBox.Text;
                    _studijuProgramma.Sifrs = sifrsTextBox.Text;
                    database.StudijuProgrammaSet.Add(_studijuProgramma);
                    database.SaveChanges();
                    Close();
               }
               else
               {
                    studijuProgramma.Nosaukums = nameTextBox.Text;
                    studijuProgramma.Sifrs = sifrsTextBox.Text;
                    database.Entry(studijuProgramma).State = System.Data.Entity.EntityState.Modified;
                    database.SaveChanges();
                    Close();
               }
          }

          private void cancelBtn_Click(object sender, EventArgs e)
          {
               Close();
          }
     }
}
