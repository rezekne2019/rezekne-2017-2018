﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SarakstaEntites;

namespace LekcijuSaraksts
{
    public partial class LekcijuKursuDialogs : Form
    {
        private readonly SarakstaEntitesContainer database;
        private readonly LekcijuKurss lekcijukurss;

        public LekcijuKursuDialogs(SarakstaEntitesContainer database, LekcijuKurss lekcijukurss = null)
        {
            this.database = database;
            this.lekcijukurss = lekcijukurss;
            InitializeComponent();

            if (lekcijukurss == null)
                this.Text = "Pievienot Lekciju Kursu";
            else
            {
                Text = "Rediģēt studiju programmu";
                LekcijuKurss_txtbox.Text = lekcijukurss.Nosaukums;
                LekcijuSifrs_txtbox.Text = lekcijukurss.Sifrs;
            }
        }

        private void btn_d_save_Click(object sender, EventArgs e)
        {

            if (string.IsNullOrWhiteSpace(LekcijuKurss_txtbox.Text) || string.IsNullOrWhiteSpace(LekcijuSifrs_txtbox.Text))
            {
                MessageBox.Show("Visi lauki nav aizpilditi");
                return;
            }

            if (lekcijukurss == null)
            {
                if (database.LekcijuKurssSet.Any(x => x.Sifrs == LekcijuSifrs_txtbox.Text))
                {
                    MessageBox.Show("Ieraksts jau pastav", "Pievienosana nav iespejama!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }

                var _lekcijukurs = new LekcijuKurss();
                _lekcijukurs.Nosaukums = LekcijuKurss_txtbox.Text;
                _lekcijukurs.Sifrs = LekcijuSifrs_txtbox.Text;
                database.LekcijuKurssSet.Add(_lekcijukurs);
                database.SaveChanges();
                Close();
            }
            else
            {
                lekcijukurss.Nosaukums = LekcijuKurss_txtbox.Text;
                lekcijukurss.Sifrs = LekcijuSifrs_txtbox.Text;
                database.Entry(lekcijukurss).State = System.Data.Entity.EntityState.Modified;
                database.SaveChanges();
                Close();
            }
        }

        private void btn_d_cancel_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
