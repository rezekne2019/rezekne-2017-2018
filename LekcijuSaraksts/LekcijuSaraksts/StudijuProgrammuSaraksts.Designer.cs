﻿namespace LekcijuSaraksts
{
     partial class StudijuProgrammuSaraksts
     {
          /// <summary>
          /// Required designer variable.
          /// </summary>
          private System.ComponentModel.IContainer components = null;

          /// <summary>
          /// Clean up any resources being used.
          /// </summary>
          /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
          protected override void Dispose(bool disposing)
          {
               if (disposing && (components != null))
               {
                    components.Dispose();
               }
               base.Dispose(disposing);
          }

          #region Windows Form Designer generated code

          /// <summary>
          /// Required method for Designer support - do not modify
          /// the contents of this method with the code editor.
          /// </summary>
          private void InitializeComponent()
          {
               this.studijuProgrammaTable = new System.Windows.Forms.DataGridView();
               this.deleteBtn = new System.Windows.Forms.Button();
               this.editBtn = new System.Windows.Forms.Button();
               this.addBtn = new System.Windows.Forms.Button();
               ((System.ComponentModel.ISupportInitialize)(this.studijuProgrammaTable)).BeginInit();
               this.SuspendLayout();
               // 
               // studijuProgrammaTable
               // 
               this.studijuProgrammaTable.AllowUserToAddRows = false;
               this.studijuProgrammaTable.AllowUserToDeleteRows = false;
               this.studijuProgrammaTable.AllowUserToOrderColumns = true;
               this.studijuProgrammaTable.AllowUserToResizeRows = false;
               this.studijuProgrammaTable.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
               this.studijuProgrammaTable.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
               this.studijuProgrammaTable.BorderStyle = System.Windows.Forms.BorderStyle.None;
               this.studijuProgrammaTable.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
               this.studijuProgrammaTable.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
               this.studijuProgrammaTable.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
               this.studijuProgrammaTable.Location = new System.Drawing.Point(13, 13);
               this.studijuProgrammaTable.MultiSelect = false;
               this.studijuProgrammaTable.Name = "studijuProgrammaTable";
               this.studijuProgrammaTable.ReadOnly = true;
               this.studijuProgrammaTable.RowHeadersVisible = false;
               this.studijuProgrammaTable.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
               this.studijuProgrammaTable.Size = new System.Drawing.Size(534, 258);
               this.studijuProgrammaTable.TabIndex = 0;
               // 
               // deleteBtn
               // 
               this.deleteBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
               this.deleteBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(149)))), ((int)(((byte)(0)))), ((int)(((byte)(4)))));
               this.deleteBtn.FlatAppearance.BorderSize = 0;
               this.deleteBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
               this.deleteBtn.ForeColor = System.Drawing.Color.White;
               this.deleteBtn.Location = new System.Drawing.Point(174, 277);
               this.deleteBtn.Name = "deleteBtn";
               this.deleteBtn.Size = new System.Drawing.Size(75, 23);
               this.deleteBtn.TabIndex = 6;
               this.deleteBtn.Text = "DZĒST";
               this.deleteBtn.UseVisualStyleBackColor = false;
               this.deleteBtn.Click += new System.EventHandler(this.deleteBtn_Click);
               // 
               // editBtn
               // 
               this.editBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
               this.editBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(16)))), ((int)(((byte)(125)))), ((int)(((byte)(190)))));
               this.editBtn.FlatAppearance.BorderSize = 0;
               this.editBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
               this.editBtn.ForeColor = System.Drawing.Color.White;
               this.editBtn.Location = new System.Drawing.Point(93, 277);
               this.editBtn.Name = "editBtn";
               this.editBtn.Size = new System.Drawing.Size(75, 23);
               this.editBtn.TabIndex = 5;
               this.editBtn.Text = "REDIĢĒT";
               this.editBtn.UseVisualStyleBackColor = false;
               this.editBtn.Click += new System.EventHandler(this.editBtn_Click);
               // 
               // addBtn
               // 
               this.addBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
               this.addBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(38)))), ((int)(((byte)(134)))), ((int)(((byte)(55)))));
               this.addBtn.FlatAppearance.BorderSize = 0;
               this.addBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
               this.addBtn.ForeColor = System.Drawing.Color.White;
               this.addBtn.Location = new System.Drawing.Point(12, 277);
               this.addBtn.Name = "addBtn";
               this.addBtn.Size = new System.Drawing.Size(75, 23);
               this.addBtn.TabIndex = 4;
               this.addBtn.Text = "PIEVIENOT";
               this.addBtn.UseVisualStyleBackColor = false;
               this.addBtn.Click += new System.EventHandler(this.addBtn_Click);
               // 
               // StudijuProgrammuSaraksts
               // 
               this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
               this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
               this.ClientSize = new System.Drawing.Size(559, 312);
               this.Controls.Add(this.deleteBtn);
               this.Controls.Add(this.editBtn);
               this.Controls.Add(this.addBtn);
               this.Controls.Add(this.studijuProgrammaTable);
               this.Name = "StudijuProgrammuSaraksts";
               this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
               this.Text = "Studiju programmas";
               ((System.ComponentModel.ISupportInitialize)(this.studijuProgrammaTable)).EndInit();
               this.ResumeLayout(false);

          }

          #endregion

          private System.Windows.Forms.DataGridView studijuProgrammaTable;
          private System.Windows.Forms.Button deleteBtn;
          private System.Windows.Forms.Button editBtn;
          private System.Windows.Forms.Button addBtn;
     }
}