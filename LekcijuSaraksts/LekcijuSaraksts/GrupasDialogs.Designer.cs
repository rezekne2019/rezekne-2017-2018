﻿namespace LekcijuSaraksts
{
    partial class GrupasDialogs
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.semestrisCbo = new System.Windows.Forms.ComboBox();
            this.semestrisLbl = new System.Windows.Forms.Label();
            this.studijuProgrammaLbl = new System.Windows.Forms.Label();
            this.studijuProgrammaCbo = new System.Windows.Forms.ComboBox();
            this.jaunaBtn = new System.Windows.Forms.Button();
            this.saglabatBtn = new System.Windows.Forms.Button();
            this.atceltBtn = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // semestrisCbo
            // 
            this.semestrisCbo.FormattingEnabled = true;
            this.semestrisCbo.Location = new System.Drawing.Point(12, 25);
            this.semestrisCbo.Name = "semestrisCbo";
            this.semestrisCbo.Size = new System.Drawing.Size(257, 21);
            this.semestrisCbo.TabIndex = 0;
            // 
            // semestrisLbl
            // 
            this.semestrisLbl.AutoSize = true;
            this.semestrisLbl.Location = new System.Drawing.Point(12, 9);
            this.semestrisLbl.Name = "semestrisLbl";
            this.semestrisLbl.Size = new System.Drawing.Size(52, 13);
            this.semestrisLbl.TabIndex = 1;
            this.semestrisLbl.Text = "Semestris";
            // 
            // studijuProgrammaLbl
            // 
            this.studijuProgrammaLbl.AutoSize = true;
            this.studijuProgrammaLbl.Location = new System.Drawing.Point(12, 46);
            this.studijuProgrammaLbl.Name = "studijuProgrammaLbl";
            this.studijuProgrammaLbl.Size = new System.Drawing.Size(95, 13);
            this.studijuProgrammaLbl.TabIndex = 2;
            this.studijuProgrammaLbl.Text = "Studiju Programma";
            // 
            // studijuProgrammaCbo
            // 
            this.studijuProgrammaCbo.FormattingEnabled = true;
            this.studijuProgrammaCbo.Location = new System.Drawing.Point(12, 62);
            this.studijuProgrammaCbo.Name = "studijuProgrammaCbo";
            this.studijuProgrammaCbo.Size = new System.Drawing.Size(257, 21);
            this.studijuProgrammaCbo.TabIndex = 3;
            // 
            // jaunaBtn
            // 
            this.jaunaBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.jaunaBtn.FlatAppearance.BorderSize = 0;
            this.jaunaBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.jaunaBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.jaunaBtn.ForeColor = System.Drawing.Color.White;
            this.jaunaBtn.Location = new System.Drawing.Point(12, 89);
            this.jaunaBtn.Name = "jaunaBtn";
            this.jaunaBtn.Size = new System.Drawing.Size(156, 37);
            this.jaunaBtn.TabIndex = 7;
            this.jaunaBtn.Text = "JAUNA STUDIJU PROGRAMMA";
            this.jaunaBtn.UseVisualStyleBackColor = false;
            this.jaunaBtn.Click += new System.EventHandler(this.jaunaBtn_Click);
            // 
            // saglabatBtn
            // 
            this.saglabatBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(16)))), ((int)(((byte)(125)))), ((int)(((byte)(190)))));
            this.saglabatBtn.FlatAppearance.BorderSize = 0;
            this.saglabatBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.saglabatBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.saglabatBtn.ForeColor = System.Drawing.Color.White;
            this.saglabatBtn.Location = new System.Drawing.Point(12, 132);
            this.saglabatBtn.Name = "saglabatBtn";
            this.saglabatBtn.Size = new System.Drawing.Size(75, 23);
            this.saglabatBtn.TabIndex = 8;
            this.saglabatBtn.Text = "SAGLABĀT";
            this.saglabatBtn.UseVisualStyleBackColor = false;
            this.saglabatBtn.Click += new System.EventHandler(this.saglabatBtn_Click);
            // 
            // atceltBtn
            // 
            this.atceltBtn.BackColor = System.Drawing.Color.Gray;
            this.atceltBtn.FlatAppearance.BorderSize = 0;
            this.atceltBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.atceltBtn.ForeColor = System.Drawing.Color.White;
            this.atceltBtn.Location = new System.Drawing.Point(93, 132);
            this.atceltBtn.Name = "atceltBtn";
            this.atceltBtn.Size = new System.Drawing.Size(75, 23);
            this.atceltBtn.TabIndex = 9;
            this.atceltBtn.Text = "ATCELT";
            this.atceltBtn.UseVisualStyleBackColor = false;
            this.atceltBtn.Click += new System.EventHandler(this.atceltBtn_Click);
            // 
            // GrupasDialogs
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 197);
            this.Controls.Add(this.atceltBtn);
            this.Controls.Add(this.saglabatBtn);
            this.Controls.Add(this.jaunaBtn);
            this.Controls.Add(this.studijuProgrammaCbo);
            this.Controls.Add(this.studijuProgrammaLbl);
            this.Controls.Add(this.semestrisLbl);
            this.Controls.Add(this.semestrisCbo);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "GrupasDialogs";
            this.Text = "GrupasDialogs";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox semestrisCbo;
        private System.Windows.Forms.Label semestrisLbl;
        private System.Windows.Forms.Label studijuProgrammaLbl;
        private System.Windows.Forms.ComboBox studijuProgrammaCbo;
        private System.Windows.Forms.Button jaunaBtn;
        private System.Windows.Forms.Button saglabatBtn;
        private System.Windows.Forms.Button atceltBtn;
    }
}