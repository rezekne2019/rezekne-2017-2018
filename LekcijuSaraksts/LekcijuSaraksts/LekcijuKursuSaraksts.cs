﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SarakstaEntites;

namespace LekcijuSaraksts
{
    public partial class LekcijuKursuSaraksts : Form
    {
        private SarakstaEntitesContainer database;
        public LekcijuKursuSaraksts(SarakstaEntitesContainer database) {
            this.database = database;

            InitializeComponent();

            RefreshTable();
        }

        private void RefreshTable() {
            LekcijuKursuSarakstsData.DataSource = database.LekcijuKurssSet.ToList();
            LekcijuKursuSarakstsData.Columns[0].Visible = false;
            LekcijuKursuSarakstsData.Columns[1].HeaderText = "Lekciju Kurss";
            LekcijuKursuSarakstsData.Columns[2].HeaderText = "Sifrs / kods";
            LekcijuKursuSarakstsData.Columns[3].Visible = false;
        }

        private LekcijuKurss GetLekcijuKursu() {
            if (LekcijuKursuSarakstsData.SelectedCells.Count <= 0)
                return null;

            int idx = LekcijuKursuSarakstsData.SelectedCells[0].RowIndex;

            DataGridViewRow row = LekcijuKursuSarakstsData.Rows[idx];

            int a = Convert.ToInt32(row.Cells["Id"].Value);
            LekcijuKurss entity = database.LekcijuKurssSet.SingleOrDefault(x => x.Id == a);
            return entity;
        }

        private void btn_delete_Click(object sender, EventArgs e) {
            var LK = GetLekcijuKursu();
            if (LK == null)
                return;
            var choice = MessageBox.Show(
                $"Dzēst lekciju kursu {LK.Nosaukums} {LK.Sifrs}?",
                "Deletot lekciju kursu???",
                MessageBoxButtons.YesNo,
                MessageBoxIcon.Warning);

            if (choice == DialogResult.Yes)
            {
                database.LekcijuKurssSet.Remove(LK);
                database.SaveChanges();
                RefreshTable();
            }
        }

        private void btn_add_Click(object sender, EventArgs e) {
            LekcijuKursuDialogs dialog = new LekcijuKursuDialogs(database);
            dialog.FormClosed += delegate { RefreshTable(); };
            dialog.ShowDialog();
        }

        private void btn_change_Click(object sender, EventArgs e)
        {
            LekcijuKurss LK = GetLekcijuKursu();
            if (LK == null)
                return;
            var dialog = new LekcijuKursuDialogs(database, LK);
            dialog.FormClosed += delegate { RefreshTable(); };
            dialog.ShowDialog();
        }
    }
}
