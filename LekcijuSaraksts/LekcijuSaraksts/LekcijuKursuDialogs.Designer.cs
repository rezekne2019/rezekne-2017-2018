﻿namespace LekcijuSaraksts
{
    partial class LekcijuKursuDialogs
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.LekcijuKurss_txtbox = new System.Windows.Forms.TextBox();
            this.LekcijuSifrs_txtbox = new System.Windows.Forms.TextBox();
            this.btn_d_save = new System.Windows.Forms.Button();
            this.btn_d_cancel = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // LekcijuKurss_txtbox
            // 
            this.LekcijuKurss_txtbox.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.LekcijuKurss_txtbox.Location = new System.Drawing.Point(20, 39);
            this.LekcijuKurss_txtbox.Name = "LekcijuKurss_txtbox";
            this.LekcijuKurss_txtbox.Size = new System.Drawing.Size(225, 23);
            this.LekcijuKurss_txtbox.TabIndex = 0;
            // 
            // LekcijuSifrs_txtbox
            // 
            this.LekcijuSifrs_txtbox.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.LekcijuSifrs_txtbox.Location = new System.Drawing.Point(20, 106);
            this.LekcijuSifrs_txtbox.Name = "LekcijuSifrs_txtbox";
            this.LekcijuSifrs_txtbox.Size = new System.Drawing.Size(225, 23);
            this.LekcijuSifrs_txtbox.TabIndex = 1;
            // 
            // btn_d_save
            // 
            this.btn_d_save.BackColor = System.Drawing.Color.Chartreuse;
            this.btn_d_save.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_d_save.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btn_d_save.ForeColor = System.Drawing.Color.Black;
            this.btn_d_save.Location = new System.Drawing.Point(20, 161);
            this.btn_d_save.Name = "btn_d_save";
            this.btn_d_save.Size = new System.Drawing.Size(91, 38);
            this.btn_d_save.TabIndex = 2;
            this.btn_d_save.Text = "SAVE";
            this.btn_d_save.UseVisualStyleBackColor = false;
            this.btn_d_save.Click += new System.EventHandler(this.btn_d_save_Click);
            // 
            // btn_d_cancel
            // 
            this.btn_d_cancel.BackColor = System.Drawing.Color.Gold;
            this.btn_d_cancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_d_cancel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btn_d_cancel.Location = new System.Drawing.Point(156, 161);
            this.btn_d_cancel.Name = "btn_d_cancel";
            this.btn_d_cancel.Size = new System.Drawing.Size(89, 38);
            this.btn_d_cancel.TabIndex = 3;
            this.btn_d_cancel.Text = "CANCEL";
            this.btn_d_cancel.UseVisualStyleBackColor = false;
            this.btn_d_cancel.Click += new System.EventHandler(this.btn_d_cancel_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(39, 19);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(95, 18);
            this.label1.TabIndex = 4;
            this.label1.Text = "Lekciju kurss";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.Location = new System.Drawing.Point(39, 86);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(128, 18);
            this.label2.TabIndex = 5;
            this.label2.Text = "Šifrs / lekciju kods";
            // 
            // LekcijuKursuDialogs
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(313, 211);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btn_d_cancel);
            this.Controls.Add(this.btn_d_save);
            this.Controls.Add(this.LekcijuSifrs_txtbox);
            this.Controls.Add(this.LekcijuKurss_txtbox);
            this.Name = "LekcijuKursuDialogs";
            this.Text = "LekcijuKursuDialogs";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox LekcijuKurss_txtbox;
        private System.Windows.Forms.TextBox LekcijuSifrs_txtbox;
        private System.Windows.Forms.Button btn_d_save;
        private System.Windows.Forms.Button btn_d_cancel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
    }
}