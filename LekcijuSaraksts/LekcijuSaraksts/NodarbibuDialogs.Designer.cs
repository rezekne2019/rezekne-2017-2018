﻿namespace LekcijuSaraksts
{
    partial class NodarbibuDialogs
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.DienaLabel = new System.Windows.Forms.Label();
            this.DienaCombo = new System.Windows.Forms.ComboBox();
            this.LaiksCombo = new System.Windows.Forms.ComboBox();
            this.LaiksLabel = new System.Windows.Forms.Label();
            this.TelpaCombo = new System.Windows.Forms.ComboBox();
            this.TelpaLabel = new System.Windows.Forms.Label();
            this.DocetajsCombo = new System.Windows.Forms.ComboBox();
            this.DocetajsLabel = new System.Windows.Forms.Label();
            this.KurssCombo = new System.Windows.Forms.ComboBox();
            this.KurssLabel = new System.Windows.Forms.Label();
            this.ProgrammaCombo = new System.Windows.Forms.ComboBox();
            this.ProgrammaLabel = new System.Windows.Forms.Label();
            this.saglabatBtn = new System.Windows.Forms.Button();
            this.atceltBtn = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // DienaLabel
            // 
            this.DienaLabel.AutoSize = true;
            this.DienaLabel.Location = new System.Drawing.Point(28, 36);
            this.DienaLabel.Name = "DienaLabel";
            this.DienaLabel.Size = new System.Drawing.Size(35, 13);
            this.DienaLabel.TabIndex = 0;
            this.DienaLabel.Text = "Diena";
            // 
            // DienaCombo
            // 
            this.DienaCombo.FormattingEnabled = true;
            this.DienaCombo.Location = new System.Drawing.Point(31, 52);
            this.DienaCombo.Name = "DienaCombo";
            this.DienaCombo.Size = new System.Drawing.Size(121, 21);
            this.DienaCombo.TabIndex = 1;
            // 
            // LaiksCombo
            // 
            this.LaiksCombo.FormattingEnabled = true;
            this.LaiksCombo.Location = new System.Drawing.Point(31, 93);
            this.LaiksCombo.Name = "LaiksCombo";
            this.LaiksCombo.Size = new System.Drawing.Size(121, 21);
            this.LaiksCombo.TabIndex = 3;
            // 
            // LaiksLabel
            // 
            this.LaiksLabel.AutoSize = true;
            this.LaiksLabel.Location = new System.Drawing.Point(28, 76);
            this.LaiksLabel.Name = "LaiksLabel";
            this.LaiksLabel.Size = new System.Drawing.Size(32, 13);
            this.LaiksLabel.TabIndex = 2;
            this.LaiksLabel.Text = "Laiks";
            // 
            // TelpaCombo
            // 
            this.TelpaCombo.FormattingEnabled = true;
            this.TelpaCombo.Location = new System.Drawing.Point(31, 133);
            this.TelpaCombo.Name = "TelpaCombo";
            this.TelpaCombo.Size = new System.Drawing.Size(121, 21);
            this.TelpaCombo.TabIndex = 5;
            this.TelpaCombo.Format += new System.Windows.Forms.ListControlConvertEventHandler(this.TelpaCombo_Format);
            // 
            // TelpaLabel
            // 
            this.TelpaLabel.AutoSize = true;
            this.TelpaLabel.Location = new System.Drawing.Point(28, 117);
            this.TelpaLabel.Name = "TelpaLabel";
            this.TelpaLabel.Size = new System.Drawing.Size(34, 13);
            this.TelpaLabel.TabIndex = 4;
            this.TelpaLabel.Text = "Telpa";
            // 
            // DocetajsCombo
            // 
            this.DocetajsCombo.FormattingEnabled = true;
            this.DocetajsCombo.Location = new System.Drawing.Point(31, 174);
            this.DocetajsCombo.Name = "DocetajsCombo";
            this.DocetajsCombo.Size = new System.Drawing.Size(121, 21);
            this.DocetajsCombo.TabIndex = 7;
            this.DocetajsCombo.Format += new System.Windows.Forms.ListControlConvertEventHandler(this.DocetajsCombo_Format);
            // 
            // DocetajsLabel
            // 
            this.DocetajsLabel.AutoSize = true;
            this.DocetajsLabel.Location = new System.Drawing.Point(28, 158);
            this.DocetajsLabel.Name = "DocetajsLabel";
            this.DocetajsLabel.Size = new System.Drawing.Size(49, 13);
            this.DocetajsLabel.TabIndex = 6;
            this.DocetajsLabel.Text = "Docētājs";
            // 
            // KurssCombo
            // 
            this.KurssCombo.FormattingEnabled = true;
            this.KurssCombo.Location = new System.Drawing.Point(31, 215);
            this.KurssCombo.Name = "KurssCombo";
            this.KurssCombo.Size = new System.Drawing.Size(121, 21);
            this.KurssCombo.TabIndex = 9;
            // 
            // KurssLabel
            // 
            this.KurssLabel.AutoSize = true;
            this.KurssLabel.Location = new System.Drawing.Point(28, 198);
            this.KurssLabel.Name = "KurssLabel";
            this.KurssLabel.Size = new System.Drawing.Size(58, 13);
            this.KurssLabel.TabIndex = 8;
            this.KurssLabel.Text = "Priekšmets";
            // 
            // ProgrammaCombo
            // 
            this.ProgrammaCombo.FormattingEnabled = true;
            this.ProgrammaCombo.Location = new System.Drawing.Point(31, 259);
            this.ProgrammaCombo.Name = "ProgrammaCombo";
            this.ProgrammaCombo.Size = new System.Drawing.Size(121, 21);
            this.ProgrammaCombo.TabIndex = 11;
            this.ProgrammaCombo.Format += new System.Windows.Forms.ListControlConvertEventHandler(this.ProgrammaCombo_Format);
            // 
            // ProgrammaLabel
            // 
            this.ProgrammaLabel.AutoSize = true;
            this.ProgrammaLabel.Location = new System.Drawing.Point(28, 242);
            this.ProgrammaLabel.Name = "ProgrammaLabel";
            this.ProgrammaLabel.Size = new System.Drawing.Size(60, 13);
            this.ProgrammaLabel.TabIndex = 10;
            this.ProgrammaLabel.Text = "Programma";
            // 
            // saglabatBtn
            // 
            this.saglabatBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(16)))), ((int)(((byte)(125)))), ((int)(((byte)(190)))));
            this.saglabatBtn.FlatAppearance.BorderSize = 0;
            this.saglabatBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.saglabatBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.saglabatBtn.ForeColor = System.Drawing.Color.White;
            this.saglabatBtn.Location = new System.Drawing.Point(31, 328);
            this.saglabatBtn.Name = "saglabatBtn";
            this.saglabatBtn.Size = new System.Drawing.Size(75, 23);
            this.saglabatBtn.TabIndex = 12;
            this.saglabatBtn.Text = "SAGLABĀT";
            this.saglabatBtn.UseVisualStyleBackColor = false;
            this.saglabatBtn.Click += new System.EventHandler(this.saglabatBtn_Click);
            // 
            // atceltBtn
            // 
            this.atceltBtn.BackColor = System.Drawing.Color.Gray;
            this.atceltBtn.FlatAppearance.BorderSize = 0;
            this.atceltBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.atceltBtn.ForeColor = System.Drawing.Color.White;
            this.atceltBtn.Location = new System.Drawing.Point(130, 328);
            this.atceltBtn.Name = "atceltBtn";
            this.atceltBtn.Size = new System.Drawing.Size(75, 23);
            this.atceltBtn.TabIndex = 13;
            this.atceltBtn.Text = "ATCELT";
            this.atceltBtn.UseVisualStyleBackColor = false;
            this.atceltBtn.Click += new System.EventHandler(this.atceltBtn_Click);
            // 
            // NodarbibuDialogs
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(313, 363);
            this.Controls.Add(this.atceltBtn);
            this.Controls.Add(this.saglabatBtn);
            this.Controls.Add(this.ProgrammaCombo);
            this.Controls.Add(this.ProgrammaLabel);
            this.Controls.Add(this.KurssCombo);
            this.Controls.Add(this.KurssLabel);
            this.Controls.Add(this.DocetajsCombo);
            this.Controls.Add(this.DocetajsLabel);
            this.Controls.Add(this.TelpaCombo);
            this.Controls.Add(this.TelpaLabel);
            this.Controls.Add(this.LaiksCombo);
            this.Controls.Add(this.LaiksLabel);
            this.Controls.Add(this.DienaCombo);
            this.Controls.Add(this.DienaLabel);
            this.Name = "NodarbibuDialogs";
            this.Text = "NodarbibuDialogs";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label DienaLabel;
        private System.Windows.Forms.ComboBox DienaCombo;
        private System.Windows.Forms.ComboBox LaiksCombo;
        private System.Windows.Forms.Label LaiksLabel;
        private System.Windows.Forms.ComboBox TelpaCombo;
        private System.Windows.Forms.Label TelpaLabel;
        private System.Windows.Forms.ComboBox DocetajsCombo;
        private System.Windows.Forms.Label DocetajsLabel;
        private System.Windows.Forms.ComboBox KurssCombo;
        private System.Windows.Forms.Label KurssLabel;
        private System.Windows.Forms.ComboBox ProgrammaCombo;
        private System.Windows.Forms.Label ProgrammaLabel;
        private System.Windows.Forms.Button saglabatBtn;
        private System.Windows.Forms.Button atceltBtn;
    }
}