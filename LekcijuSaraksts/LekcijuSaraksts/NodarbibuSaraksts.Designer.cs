﻿namespace LekcijuSaraksts
{
    partial class NodarbibuSaraksts
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pievienotBtn = new System.Windows.Forms.Button();
            this.dzestBtn = new System.Windows.Forms.Button();
            this.NodarbibuTable = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.NodarbibuTable)).BeginInit();
            this.SuspendLayout();
            // 
            // pievienotBtn
            // 
            this.pievienotBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.pievienotBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(38)))), ((int)(((byte)(134)))), ((int)(((byte)(55)))));
            this.pievienotBtn.FlatAppearance.BorderSize = 0;
            this.pievienotBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.pievienotBtn.ForeColor = System.Drawing.Color.White;
            this.pievienotBtn.Location = new System.Drawing.Point(37, 270);
            this.pievienotBtn.Name = "pievienotBtn";
            this.pievienotBtn.Size = new System.Drawing.Size(75, 23);
            this.pievienotBtn.TabIndex = 3;
            this.pievienotBtn.Text = "PIEVIENOT";
            this.pievienotBtn.UseVisualStyleBackColor = false;
            this.pievienotBtn.Click += new System.EventHandler(this.pievienotBtn_Click);
            // 
            // dzestBtn
            // 
            this.dzestBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.dzestBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(149)))), ((int)(((byte)(0)))), ((int)(((byte)(4)))));
            this.dzestBtn.FlatAppearance.BorderSize = 0;
            this.dzestBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.dzestBtn.ForeColor = System.Drawing.Color.White;
            this.dzestBtn.Location = new System.Drawing.Point(145, 270);
            this.dzestBtn.Name = "dzestBtn";
            this.dzestBtn.Size = new System.Drawing.Size(75, 23);
            this.dzestBtn.TabIndex = 5;
            this.dzestBtn.Text = "DZĒST";
            this.dzestBtn.UseVisualStyleBackColor = false;
            this.dzestBtn.Click += new System.EventHandler(this.dzestBtn_Click);
            // 
            // NodarbibuTable
            // 
            this.NodarbibuTable.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.NodarbibuTable.Location = new System.Drawing.Point(37, 35);
            this.NodarbibuTable.Name = "NodarbibuTable";
            this.NodarbibuTable.Size = new System.Drawing.Size(797, 210);
            this.NodarbibuTable.TabIndex = 7;
            // 
            // NodarbibuSaraksts
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(868, 305);
            this.Controls.Add(this.NodarbibuTable);
            this.Controls.Add(this.dzestBtn);
            this.Controls.Add(this.pievienotBtn);
            this.Name = "NodarbibuSaraksts";
            this.Text = "NodarbibuSaraksts";
            ((System.ComponentModel.ISupportInitialize)(this.NodarbibuTable)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button pievienotBtn;
        private System.Windows.Forms.Button dzestBtn;
        private System.Windows.Forms.DataGridView NodarbibuTable;
    }
}