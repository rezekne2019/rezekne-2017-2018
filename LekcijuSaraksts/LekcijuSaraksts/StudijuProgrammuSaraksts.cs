﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using SarakstaEntites;

namespace LekcijuSaraksts
{
     public partial class StudijuProgrammuSaraksts : Form
     {
          private SarakstaEntitesContainer database;
          public StudijuProgrammuSaraksts()
          {
               InitializeComponent();
          }

          public StudijuProgrammuSaraksts(SarakstaEntitesContainer database)
          {
               this.database = database;
               InitializeComponent();
               RefreshTable();
          }

          private void RefreshTable()
          {
               studijuProgrammaTable.DataSource = database.StudijuProgrammaSet.ToList();
               studijuProgrammaTable.Columns[0].Visible = false;
               studijuProgrammaTable.Columns[1].HeaderText = "Nosaukums";
               studijuProgrammaTable.Columns[2].HeaderText = "Šifrs";
               studijuProgrammaTable.Columns[3].Visible = false;
          }

          private StudijuProgramma GetSelectedStudijuProgramma()
          {
               if (studijuProgrammaTable.SelectedCells.Count <= 0)
                    return null;

               int idx = studijuProgrammaTable.SelectedCells[0].RowIndex;

               DataGridViewRow row = studijuProgrammaTable.Rows[idx];

               int a = Convert.ToInt32(row.Cells["Id"].Value);
               StudijuProgramma entity = database.StudijuProgrammaSet.SingleOrDefault(x => x.Id == a);
               return entity;
          }

          private void deleteBtn_Click(object sender, EventArgs e)
          {
               var studProgr = GetSelectedStudijuProgramma();
               if (studProgr == null)
                    return;
               var choice = MessageBox.Show(
                   $"Dzēst studiju programmu {studProgr.Nosaukums} {studProgr.Sifrs}?",
                   "Dzēst studiju programmu?",
                   MessageBoxButtons.YesNo,
                   MessageBoxIcon.Warning);

               if (choice == DialogResult.Yes)
               {
                    database.StudijuProgrammaSet.Remove(studProgr);
                    database.SaveChanges();
                    RefreshTable();
               }
          }

          private void addBtn_Click(object sender, EventArgs e)
          {
               StudijuProgrammasDialogs dialog = new StudijuProgrammasDialogs(database);
               dialog.FormClosed += delegate { RefreshTable(); };
               dialog.ShowDialog();
          }

          private void editBtn_Click(object sender, EventArgs e)
          {
               StudijuProgramma studProgr = GetSelectedStudijuProgramma();
               if (studProgr == null)
                    return;
               var dialog = new StudijuProgrammasDialogs(database, studProgr);
               dialog.FormClosed += delegate { RefreshTable(); };
               dialog.ShowDialog();
          }
     }
}

