﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SarakstaEntites;

namespace LekcijuSaraksts
{
    public partial class NodarbibuDialogs : Form
    {
        private readonly SarakstaEntitesContainer database;
        private readonly Nodarbiba nodarbiba;

        public NodarbibuDialogs(SarakstaEntitesContainer database, Nodarbiba nodarbiba = null)
        {
            this.database = database;
            this.nodarbiba = nodarbiba;
            InitializeComponent();
            LoadDienaBox();
            LoadLaikaBox();
            LoadTelpaBox();
            LoadDocetajaBox();
            LoadKurssBox();
            LoadGrupaBox();
        }

        private void LoadDienaBox()
        {
            DienaCombo.DataSource = database.DienaSet.ToList();
            DienaCombo.DisplayMember = "Nosaukums";
            DienaCombo.DropDownStyle = ComboBoxStyle.DropDownList;
        }

        private void LoadLaikaBox()
        {
            LaiksCombo.DataSource = database.LaiksSet.ToList();
            LaiksCombo.DisplayMember = "Nodarbibas_laiks";
            LaiksCombo.DropDownStyle = ComboBoxStyle.DropDownList;
        }

        private void LoadTelpaBox()
        {
            TelpaCombo.DataSource = database.TelpaSet.ToList();
            TelpaCombo.DisplayMember = "Telpas_nr";
            TelpaCombo.DropDownStyle = ComboBoxStyle.DropDownList;
        }



        private void LoadDocetajaBox()
        {
            DocetajsCombo.DataSource = database.DocetajsSet.ToList();
            DocetajsCombo.DisplayMember = "Vards";
            DocetajsCombo.DropDownStyle = ComboBoxStyle.DropDownList;
        }

        private void LoadKurssBox()
        {
            KurssCombo.DataSource = database.LekcijuKurssSet.ToList();
            KurssCombo.DisplayMember = "Nosaukums";
            KurssCombo.DropDownStyle = ComboBoxStyle.DropDownList;
        }

        private void LoadGrupaBox()
        {
            ProgrammaCombo.DataSource = database.GrupaSet.ToList();
            ProgrammaCombo.DisplayMember = "Id";
            ProgrammaCombo.DropDownStyle = ComboBoxStyle.DropDownList;
        }

        private void DocetajsCombo_Format(object sender, ListControlConvertEventArgs e)
        {
            string vards = ((Docetajs)e.ListItem).Vards;
            string uzvards = ((Docetajs)e.ListItem).Uzvards;
            e.Value = vards + " " + uzvards;
        }

        private void saglabatBtn_Click(object sender, EventArgs e)
        {

            if (nodarbiba == null)
            {
                var nodarbiba = new Nodarbiba()
                {
                    Diena = (Diena)DienaCombo.SelectedItem,
                    Laiks = (Laiks)LaiksCombo.SelectedItem,
                    Telpa = (Telpa)TelpaCombo.SelectedItem,
                    LekcijuKurss =(LekcijuKurss)KurssCombo.SelectedItem,
                    Docetajs = (Docetajs)DocetajsCombo.SelectedItem,
                };

                var grupaId = (Grupa)ProgrammaCombo.SelectedItem;
                   
                database.NodarbibaSet.Add(nodarbiba);
                database.SaveChanges();

                var newId = nodarbiba;

                var grupnodarb = new GrupaNodarbiba
                {
                    Grupa = grupaId,
                    Nodarbiba = newId,
                };

                database.GrupaNodarbibaSet.Add(grupnodarb);
                database.SaveChanges();
            }
            else
            {
                nodarbiba.Diena = (Diena)DienaCombo.SelectedItem;
                nodarbiba.Laiks = (Laiks)LaiksCombo.SelectedItem;
                nodarbiba.Telpa = (Telpa)TelpaCombo.SelectedItem;
                nodarbiba.LekcijuKurss = (LekcijuKurss)KurssCombo.SelectedItem;
                nodarbiba.Docetajs = (Docetajs)DocetajsCombo.SelectedItem;
                database.Entry(nodarbiba).State = System.Data.Entity.EntityState.Modified;
                database.SaveChanges();
            }
            Close();
        }

        private void TelpaCombo_Format(object sender, ListControlConvertEventArgs e)
        {
            string telpa = ((Telpa)e.ListItem).Telpas_nr;
            string eka = ((Telpa)e.ListItem).Telpas_eka;
            e.Value = telpa + " " + eka;
        }

        private void atceltBtn_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void ProgrammaCombo_Format(object sender, ListControlConvertEventArgs e)
        {
            string semestris = ((Grupa)e.ListItem).Semestris;
            string studijuprog = ((Grupa)e.ListItem).StudijuProgramma.Nosaukums;
            e.Value = semestris + " " + studijuprog;
        }
    }
}
