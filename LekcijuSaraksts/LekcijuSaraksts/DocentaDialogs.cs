﻿using System;
using System.Windows.Forms;
using SarakstaEntites;

namespace LekcijuSaraksts
{
    public partial class DocentaDialogs : Form
    {
        private readonly SarakstaEntitesContainer _db;
        private readonly Docetajs _docetajs;

        public DocentaDialogs(SarakstaEntitesContainer database, Docetajs docetajs = null)
        {
            _db = database;
            _docetajs = docetajs;

            InitializeComponent();

            if (docetajs == null)
                this.Text = "Pievienot docētāju";
            else
            {
                Text = "Rediģēt docētāju";
                vardsTb.Text = docetajs.Vards;
                uzvardsTb.Text = docetajs.Uzvards;
                gradsTb.Text = docetajs.Akademiskais_grads;
            }
        }

        private void saglabatBtn_Click(object sender, EventArgs e)
        {
            if(string.IsNullOrWhiteSpace(vardsTb.Text) 
                || string.IsNullOrWhiteSpace(uzvardsTb.Text)
                || string.IsNullOrWhiteSpace(gradsTb.Text))
            {
                MessageBox.Show("Lūdzu aizpildiet visus laukus");
                return;
            }

            if (_docetajs == null) {
                var docetajs = new Docetajs();
                docetajs.Vards = vardsTb.Text;
                docetajs.Uzvards = uzvardsTb.Text;
                docetajs.Akademiskais_grads = gradsTb.Text;
                _db.DocetajsSet.Add(docetajs);
                _db.SaveChanges();
                Close();
            }
            else
            {
                _docetajs.Vards = vardsTb.Text;
                _docetajs.Uzvards = uzvardsTb.Text;
                _docetajs.Akademiskais_grads = gradsTb.Text;
                _db.Entry(_docetajs).State = System.Data.Entity.EntityState.Modified;
                _db.SaveChanges();
                Close();
            }
        }
    }
}
