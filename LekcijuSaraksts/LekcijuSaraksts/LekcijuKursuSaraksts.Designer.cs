﻿namespace LekcijuSaraksts
{
    partial class LekcijuKursuSaraksts
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_add = new System.Windows.Forms.Button();
            this.btn_change = new System.Windows.Forms.Button();
            this.btn_delete = new System.Windows.Forms.Button();
            this.LekcijuKursuSarakstsData = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.LekcijuKursuSarakstsData)).BeginInit();
            this.SuspendLayout();
            // 
            // btn_add
            // 
            this.btn_add.BackColor = System.Drawing.Color.LimeGreen;
            this.btn_add.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btn_add.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btn_add.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btn_add.Location = new System.Drawing.Point(33, 318);
            this.btn_add.Name = "btn_add";
            this.btn_add.Size = new System.Drawing.Size(105, 30);
            this.btn_add.TabIndex = 4;
            this.btn_add.Text = "PIEVIENOT";
            this.btn_add.UseVisualStyleBackColor = false;
            this.btn_add.Click += new System.EventHandler(this.btn_add_Click);
            // 
            // btn_change
            // 
            this.btn_change.BackColor = System.Drawing.Color.Gold;
            this.btn_change.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_change.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btn_change.Location = new System.Drawing.Point(169, 318);
            this.btn_change.Name = "btn_change";
            this.btn_change.Size = new System.Drawing.Size(86, 30);
            this.btn_change.TabIndex = 5;
            this.btn_change.Text = "MAINĪT";
            this.btn_change.UseVisualStyleBackColor = false;
            this.btn_change.Click += new System.EventHandler(this.btn_change_Click);
            // 
            // btn_delete
            // 
            this.btn_delete.BackColor = System.Drawing.Color.Crimson;
            this.btn_delete.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_delete.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btn_delete.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btn_delete.Location = new System.Drawing.Point(291, 318);
            this.btn_delete.Name = "btn_delete";
            this.btn_delete.Size = new System.Drawing.Size(80, 30);
            this.btn_delete.TabIndex = 6;
            this.btn_delete.Text = "DZĒST";
            this.btn_delete.UseVisualStyleBackColor = false;
            this.btn_delete.Click += new System.EventHandler(this.btn_delete_Click);
            // 
            // LekcijuKursuSarakstsData
            // 
            this.LekcijuKursuSarakstsData.AllowUserToAddRows = false;
            this.LekcijuKursuSarakstsData.AllowUserToDeleteRows = false;
            this.LekcijuKursuSarakstsData.AllowUserToOrderColumns = true;
            this.LekcijuKursuSarakstsData.AllowUserToResizeRows = false;
            this.LekcijuKursuSarakstsData.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.LekcijuKursuSarakstsData.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.LekcijuKursuSarakstsData.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.LekcijuKursuSarakstsData.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.LekcijuKursuSarakstsData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.LekcijuKursuSarakstsData.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.LekcijuKursuSarakstsData.Location = new System.Drawing.Point(0, 2);
            this.LekcijuKursuSarakstsData.MultiSelect = false;
            this.LekcijuKursuSarakstsData.Name = "LekcijuKursuSarakstsData";
            this.LekcijuKursuSarakstsData.ReadOnly = true;
            this.LekcijuKursuSarakstsData.RowHeadersVisible = false;
            this.LekcijuKursuSarakstsData.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.LekcijuKursuSarakstsData.Size = new System.Drawing.Size(486, 308);
            this.LekcijuKursuSarakstsData.TabIndex = 0;
            // 
            // LekcijuKursuSaraksts
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(486, 360);
            this.Controls.Add(this.LekcijuKursuSarakstsData);
            this.Controls.Add(this.btn_delete);
            this.Controls.Add(this.btn_change);
            this.Controls.Add(this.btn_add);
            this.Name = "LekcijuKursuSaraksts";
            this.Text = "LekcijuKursuSaraksts";
            ((System.ComponentModel.ISupportInitialize)(this.LekcijuKursuSarakstsData)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Button btn_add;
        private System.Windows.Forms.Button btn_change;
        private System.Windows.Forms.Button btn_delete;
        private System.Windows.Forms.DataGridView LekcijuKursuSarakstsData;
    }
}