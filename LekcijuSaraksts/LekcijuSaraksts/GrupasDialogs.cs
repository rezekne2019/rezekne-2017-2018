﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SarakstaEntites;

namespace LekcijuSaraksts
{
    public partial class GrupasDialogs : Form
    {
        private readonly SarakstaEntitesContainer database;
        private readonly Grupa grupa;

        public GrupasDialogs(SarakstaEntitesContainer database, Grupa grupa = null)
        {
            this.database = database;
            this.grupa = grupa;

            InitializeComponent();

            LoadSemestrisBox();
            LoadProgrammaBox();
            if (grupa == null)
                this.Text = "Pievienot grupu";
            else
            {
                this.Text = "Rediģēt grupu";
                semestrisCbo.Text = grupa.Semestris;
                studijuProgrammaCbo.Text = grupa.StudijuProgramma.Nosaukums;
            }
        }

        private void LoadSemestrisBox()
        {
            var semestri = new[] { 1, 2, 3, 4, 5, 6, 7, 8 };
            semestrisCbo.DataSource = semestri;
            semestrisCbo.DropDownStyle = ComboBoxStyle.DropDownList;
        }

        private void LoadProgrammaBox()
        {
            var programmas = database.StudijuProgrammaSet.ToList();
            studijuProgrammaCbo.DataSource = programmas;
            studijuProgrammaCbo.DisplayMember = "Nosaukums";
            studijuProgrammaCbo.DropDownStyle = ComboBoxStyle.DropDownList;
        }

        private void jaunaBtn_Click(object sender, EventArgs e)
        {
           var studijuprog =  new StudijuProgrammasDialogs(database);
           studijuprog.FormClosed += delegate {
               LoadProgrammaBox();
               studijuProgrammaCbo.SelectedIndex = studijuProgrammaCbo.Items.Count -1; // Pārāda pēdējo pievienoto elementu
           };
           studijuprog.ShowDialog();
        }

        private StudijuProgramma GetSelectedStudijuProgramma()
        {
            string nosaukums = studijuProgrammaCbo.Text;
            StudijuProgramma entity = database.StudijuProgrammaSet.SingleOrDefault(x => x.Nosaukums == nosaukums);
            return entity;
        }

        private void saglabatBtn_Click(object sender, EventArgs e)
        {

            if (database.GrupaSet.Any(x => x.Semestris == semestrisCbo.Text && x.StudijuProgramma.Nosaukums == studijuProgrammaCbo.Text )) // Pārbauda vai datubāzē eksistē tāds ieraksts
            {
                MessageBox.Show("Ieraksts jau eksistē!",
                    "Nevar pievienot!",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Warning );
                return;
            }
            if (grupa == null)
            {
                var grupa = new Grupa()
                {
                    Semestris = semestrisCbo.Text,
                    StudijuProgramma = GetSelectedStudijuProgramma()
                };
                database.GrupaSet.Add(grupa);
                database.SaveChanges();
            }
            else
            {
                grupa.Semestris = semestrisCbo.Text;
                grupa.StudijuProgramma = GetSelectedStudijuProgramma();
                database.Entry(grupa).State = System.Data.Entity.EntityState.Modified;
                database.SaveChanges();
            }
            Close();
        }

        private void atceltBtn_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
