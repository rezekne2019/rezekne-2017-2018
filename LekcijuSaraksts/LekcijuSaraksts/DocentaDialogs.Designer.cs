﻿namespace LekcijuSaraksts
{
    partial class DocentaDialogs
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DocentaDialogs));
            this.vardsTb = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.atceltBtn = new System.Windows.Forms.Button();
            this.saglabatBtn = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.gradsTb = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.uzvardsTb = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // vardsTb
            // 
            this.vardsTb.Location = new System.Drawing.Point(15, 25);
            this.vardsTb.Name = "vardsTb";
            this.vardsTb.Size = new System.Drawing.Size(320, 20);
            this.vardsTb.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(34, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Vārds";
            // 
            // atceltBtn
            // 
            this.atceltBtn.BackColor = System.Drawing.Color.Gray;
            this.atceltBtn.FlatAppearance.BorderSize = 0;
            this.atceltBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.atceltBtn.ForeColor = System.Drawing.Color.White;
            this.atceltBtn.Location = new System.Drawing.Point(96, 136);
            this.atceltBtn.Name = "atceltBtn";
            this.atceltBtn.Size = new System.Drawing.Size(75, 23);
            this.atceltBtn.TabIndex = 7;
            this.atceltBtn.Text = "ATCELT";
            this.atceltBtn.UseVisualStyleBackColor = false;
            // 
            // saglabatBtn
            // 
            this.saglabatBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(16)))), ((int)(((byte)(125)))), ((int)(((byte)(190)))));
            this.saglabatBtn.FlatAppearance.BorderSize = 0;
            this.saglabatBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.saglabatBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.saglabatBtn.ForeColor = System.Drawing.Color.White;
            this.saglabatBtn.Location = new System.Drawing.Point(15, 136);
            this.saglabatBtn.Name = "saglabatBtn";
            this.saglabatBtn.Size = new System.Drawing.Size(75, 23);
            this.saglabatBtn.TabIndex = 6;
            this.saglabatBtn.Text = "SAGLABĀT";
            this.saglabatBtn.UseVisualStyleBackColor = false;
            this.saglabatBtn.Click += new System.EventHandler(this.saglabatBtn_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 87);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(35, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Grāds";
            // 
            // gradsTb
            // 
            this.gradsTb.Location = new System.Drawing.Point(15, 103);
            this.gradsTb.Name = "gradsTb";
            this.gradsTb.Size = new System.Drawing.Size(320, 20);
            this.gradsTb.TabIndex = 4;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 48);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(46, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Uzvārds";
            // 
            // uzvardsTb
            // 
            this.uzvardsTb.Location = new System.Drawing.Point(15, 64);
            this.uzvardsTb.Name = "uzvardsTb";
            this.uzvardsTb.Size = new System.Drawing.Size(320, 20);
            this.uzvardsTb.TabIndex = 2;
            // 
            // DocentaDialogs
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(352, 166);
            this.Controls.Add(this.atceltBtn);
            this.Controls.Add(this.saglabatBtn);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.gradsTb);
            this.Controls.Add(this.vardsTb);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.uzvardsTb);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "DocentaDialogs";
            this.ShowIcon = false;
            this.Text = "DocentaDialogs";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox vardsTb;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button atceltBtn;
        private System.Windows.Forms.Button saglabatBtn;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox gradsTb;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox uzvardsTb;
    }
}