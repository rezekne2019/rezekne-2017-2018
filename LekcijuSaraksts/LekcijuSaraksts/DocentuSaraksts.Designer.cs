﻿namespace LekcijuSaraksts
{
    partial class DocentuSaraksts
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.docentuSarakstsTable = new System.Windows.Forms.DataGridView();
            this.pievienotBtn = new System.Windows.Forms.Button();
            this.redigetBtn = new System.Windows.Forms.Button();
            this.dzestBtn = new System.Windows.Forms.Button();
            this.atjaunotBtn = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.docentuSarakstsTable)).BeginInit();
            this.SuspendLayout();
            // 
            // docentuSarakstsTable
            // 
            this.docentuSarakstsTable.AllowUserToAddRows = false;
            this.docentuSarakstsTable.AllowUserToDeleteRows = false;
            this.docentuSarakstsTable.AllowUserToOrderColumns = true;
            this.docentuSarakstsTable.AllowUserToResizeRows = false;
            this.docentuSarakstsTable.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.docentuSarakstsTable.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.docentuSarakstsTable.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.docentuSarakstsTable.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.docentuSarakstsTable.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.docentuSarakstsTable.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.docentuSarakstsTable.Location = new System.Drawing.Point(13, 13);
            this.docentuSarakstsTable.MultiSelect = false;
            this.docentuSarakstsTable.Name = "docentuSarakstsTable";
            this.docentuSarakstsTable.ReadOnly = true;
            this.docentuSarakstsTable.RowHeadersVisible = false;
            this.docentuSarakstsTable.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.docentuSarakstsTable.Size = new System.Drawing.Size(534, 258);
            this.docentuSarakstsTable.TabIndex = 0;
            // 
            // pievienotBtn
            // 
            this.pievienotBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.pievienotBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(38)))), ((int)(((byte)(134)))), ((int)(((byte)(55)))));
            this.pievienotBtn.FlatAppearance.BorderSize = 0;
            this.pievienotBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.pievienotBtn.ForeColor = System.Drawing.Color.White;
            this.pievienotBtn.Location = new System.Drawing.Point(13, 281);
            this.pievienotBtn.Name = "pievienotBtn";
            this.pievienotBtn.Size = new System.Drawing.Size(75, 23);
            this.pievienotBtn.TabIndex = 1;
            this.pievienotBtn.Text = "PIEVIENOT";
            this.pievienotBtn.UseVisualStyleBackColor = false;
            this.pievienotBtn.Click += new System.EventHandler(this.pievienotBtn_Click);
            // 
            // redigetBtn
            // 
            this.redigetBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.redigetBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(16)))), ((int)(((byte)(125)))), ((int)(((byte)(190)))));
            this.redigetBtn.FlatAppearance.BorderSize = 0;
            this.redigetBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.redigetBtn.ForeColor = System.Drawing.Color.White;
            this.redigetBtn.Location = new System.Drawing.Point(94, 281);
            this.redigetBtn.Name = "redigetBtn";
            this.redigetBtn.Size = new System.Drawing.Size(75, 23);
            this.redigetBtn.TabIndex = 2;
            this.redigetBtn.Text = "REDIĢĒT";
            this.redigetBtn.UseVisualStyleBackColor = false;
            this.redigetBtn.Click += new System.EventHandler(this.redigetBtn_Click);
            // 
            // dzestBtn
            // 
            this.dzestBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.dzestBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(149)))), ((int)(((byte)(0)))), ((int)(((byte)(4)))));
            this.dzestBtn.FlatAppearance.BorderSize = 0;
            this.dzestBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.dzestBtn.ForeColor = System.Drawing.Color.White;
            this.dzestBtn.Location = new System.Drawing.Point(175, 281);
            this.dzestBtn.Name = "dzestBtn";
            this.dzestBtn.Size = new System.Drawing.Size(75, 23);
            this.dzestBtn.TabIndex = 3;
            this.dzestBtn.Text = "DZĒST";
            this.dzestBtn.UseVisualStyleBackColor = false;
            this.dzestBtn.Click += new System.EventHandler(this.dzestBtn_Click);
            // 
            // atjaunotBtn
            // 
            this.atjaunotBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.atjaunotBtn.BackColor = System.Drawing.Color.Gray;
            this.atjaunotBtn.FlatAppearance.BorderSize = 0;
            this.atjaunotBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.atjaunotBtn.ForeColor = System.Drawing.Color.White;
            this.atjaunotBtn.Location = new System.Drawing.Point(472, 281);
            this.atjaunotBtn.Name = "atjaunotBtn";
            this.atjaunotBtn.Size = new System.Drawing.Size(75, 23);
            this.atjaunotBtn.TabIndex = 4;
            this.atjaunotBtn.Text = "ATJAUNOT";
            this.atjaunotBtn.UseVisualStyleBackColor = false;
            this.atjaunotBtn.Click += new System.EventHandler(this.atjaunotBtn_Click);
            // 
            // DocentuSaraksts
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(559, 312);
            this.Controls.Add(this.atjaunotBtn);
            this.Controls.Add(this.dzestBtn);
            this.Controls.Add(this.redigetBtn);
            this.Controls.Add(this.pievienotBtn);
            this.Controls.Add(this.docentuSarakstsTable);
            this.Name = "DocentuSaraksts";
            this.Text = "DOCĒTĀJI";
            ((System.ComponentModel.ISupportInitialize)(this.docentuSarakstsTable)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView docentuSarakstsTable;
        private System.Windows.Forms.Button pievienotBtn;
        private System.Windows.Forms.Button redigetBtn;
        private System.Windows.Forms.Button dzestBtn;
        private System.Windows.Forms.Button atjaunotBtn;
    }
}