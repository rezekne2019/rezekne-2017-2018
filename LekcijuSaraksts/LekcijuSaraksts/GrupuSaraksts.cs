﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SarakstaEntites;

namespace LekcijuSaraksts
{
    public partial class GrupuSaraksts : Form
    {
        private readonly SarakstaEntitesContainer database;
        public GrupuSaraksts(SarakstaEntitesContainer database)
        {
            this.database = database;
            InitializeComponent();
            RefreshTable();
        }

        public void RefreshTable()
        {
            var query = from g in database.GrupaSet //veido vaicājumu grupu tabulai
                        select new
                        {
                            Id = g.Id,
                            Semestris = g.Semestris,
                            StudijuProgramma = g.StudijuProgramma.Nosaukums // izvelk studiju programmas nosaukumu no atsauces atslēgas lauka
                        };
            grupuSarakstsTable.DataSource = query.ToList();

            grupuSarakstsTable.Columns[0].Visible = false;
            grupuSarakstsTable.Columns[2].HeaderText = "Studiju programma";
        }

        private Grupa GetSelectedGrupa()
        {
            if (grupuSarakstsTable.SelectedCells.Count <= 0)
                return null;

            int idx = grupuSarakstsTable.SelectedCells[0].RowIndex;

            DataGridViewRow row = grupuSarakstsTable.Rows[idx];

            int a = Convert.ToInt32(row.Cells["Id"].Value);
            var entity = database.GrupaSet.SingleOrDefault(x => x.Id == a);
            return entity;
        }

        private void pievienotBtn_Click(object sender, EventArgs e)
        {
            var dialog = new GrupasDialogs(database);
            dialog.FormClosed += delegate { RefreshTable(); };
            dialog.ShowDialog();
        }

        private void redigetBtn_Click(object sender, EventArgs e)
        {
            Grupa grupa = GetSelectedGrupa();
            if (grupa == null)
                return;
            var dialog = new GrupasDialogs(database, grupa);
            dialog.FormClosed += delegate { RefreshTable(); };
            dialog.ShowDialog();
        }

        private void dzestBtn_Click(object sender, EventArgs e)
        {
            var grupa = GetSelectedGrupa();
            if (grupa == null)
                return;
            var choice = MessageBox.Show(
                $"Dzēst grupu {grupa.StudijuProgramma.Nosaukums} {grupa.Semestris}?",
                "Dzēst grupu?",
                MessageBoxButtons.YesNo,
                MessageBoxIcon.Warning);

            if (choice == DialogResult.Yes)
            {
                database.GrupaSet.Remove(grupa);
                database.SaveChanges();
                RefreshTable();
            }
        }
    }
}
