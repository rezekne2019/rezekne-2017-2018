﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SarakstaEntites;

namespace LekcijuSaraksts
{
    public partial class LekcijuSaraksts : Form
    {
        private SarakstaEntitesContainer database;

        public LekcijuSaraksts()
        {
            InitializeComponent();
        }

        public LekcijuSaraksts(SarakstaEntitesContainer database)
        {
            this.database = database;
            InitializeComponent();
            LoadDienasListBox();
            LoadSemestrisBox();
            LoadProgrammaBox();
        }

        private void studijuProgrammasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new StudijuProgrammuSaraksts(database).Show();
        }

        private void docētājiToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new DocentuSaraksts(database).Show();
        }

        private void grupasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new GrupuSaraksts(database).Show();
        }

        private void LoadDienasListBox()
        {
            var dienas = database.DienaSet.ToList();
            var blank = new Diena();
            blank.Nosaukums = "Visas";
            dienas.Add(blank);
            dienaComboBox.DataSource = dienas;
            dienaComboBox.DisplayMember = "Nosaukums";
            dienaComboBox.SelectedItem = dienas.Last();
        }

        private void LoadSemestrisBox()
        {
            var semestri = new[] { 1, 2, 3, 4, 5, 6, 7, 8 };
            semestrisComboBox.DataSource = semestri;
        }

        private void LoadProgrammaBox()
        {
            var programmas = database.StudijuProgrammaSet.ToList();
            programmaComboBox.DataSource = programmas;
            programmaComboBox.DisplayMember = "Nosaukums";
        }

        private void raditBtn_Click(object sender, EventArgs e)
        {
            var diena = dienaComboBox.Text;
            var semestris = semestrisComboBox.Text;
            var programma = programmaComboBox.Text;

            var nodarbibas = database.NodarbibaSet
                .Where(d => diena == "Visas" ? true : d.Diena.Nosaukums == diena)
                .Where(a => a.GrupaNodarbiba.Any(b => b.Nodarbiba.Id == a.Id &&
                b.Grupa.Semestris == semestris &&
                b.Grupa.StudijuProgramma.Nosaukums == programma))
                .Select(x => new
                {
                    Diena = x.Diena.Nosaukums,
                    Laiks = x.Laiks.Nodarbibas_laiks,
                    Kurss = x.LekcijuKurss.Nosaukums,
                    Docētajs = x.Docetajs.Vards + " " + x.Docetajs.Uzvards,
                    Telpa = x.Telpa.Telpas_nr + " (" + x.Telpa.Telpas_eka + ")"
                });

            lekcijuSarakstsTable.DataSource = nodarbibas.ToList();
        }

        private void nodarbībasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new NodarbibuSaraksts(database).Show();
        }

        private void lekcijuKursiToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new LekcijuKursuSaraksts(database).Show();
        }

        private void aboutToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            ParDialogs parDial = new ParDialogs();
            parDial.ShowDialog();

        }
    }
}
