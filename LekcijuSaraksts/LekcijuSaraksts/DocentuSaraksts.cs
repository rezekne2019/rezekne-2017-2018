﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using SarakstaEntites;

namespace LekcijuSaraksts
{
    public partial class DocentuSaraksts : Form
    {
        private SarakstaEntitesContainer _db;
        public DocentuSaraksts(SarakstaEntitesContainer database)
        {
            _db = database;
            InitializeComponent();
            RefreshTable();
        }

        private void RefreshTable()
        {
            docentuSarakstsTable.DataSource = _db.DocetajsSet.ToList(); // ielādē visus ierakstus no DB
            docentuSarakstsTable.Columns[0].Visible = false; // noslēp Id kolonnu
            docentuSarakstsTable.Columns[1].HeaderText = "Vārds";
            docentuSarakstsTable.Columns[2].HeaderText = "Uzvārds";
            docentuSarakstsTable.Columns[3].HeaderText = "Grāds";
            docentuSarakstsTable.Columns[4].Visible = false; // noslēp Nodarbība kolonnu
        }

        private Docetajs GetSelectedDocetajs()
        {
            if (docentuSarakstsTable.SelectedCells.Count <= 0)
                return null;

            int idx = docentuSarakstsTable.SelectedCells[0].RowIndex;

            DataGridViewRow row = docentuSarakstsTable.Rows[idx];

            int a = Convert.ToInt32(row.Cells["Id"].Value);
            var entity = _db.DocetajsSet.SingleOrDefault(x => x.Id == a);
            return entity;
        }

        #region GUI
        private void atjaunotBtn_Click(object sender, EventArgs e)
        {
            RefreshTable();
        }

        private void dzestBtn_Click(object sender, EventArgs e)
        {
            var doc = GetSelectedDocetajs();
            if (doc == null)
                return;
            var choice = MessageBox.Show(
                $"Dzēst docētāju {doc.Vards} {doc.Uzvards}?", 
                "Dzēst docētāju?", 
                MessageBoxButtons.YesNo, 
                MessageBoxIcon.Warning);

            if (choice == DialogResult.Yes)
            {
                _db.DocetajsSet.Remove(doc);
                _db.SaveChanges();
                RefreshTable();
            }
        }

        private void pievienotBtn_Click(object sender, EventArgs e)
        {
            var dialog = new DocentaDialogs(_db);
            dialog.FormClosed += delegate { RefreshTable(); };
            dialog.ShowDialog();
        }

        private void redigetBtn_Click(object sender, EventArgs e)
        {
            var doc = GetSelectedDocetajs();
            if (doc == null)
                return;
            var dialog = new DocentaDialogs(_db, doc);
            dialog.FormClosed += delegate { RefreshTable(); };
            dialog.ShowDialog();
        }
        #endregion

    }
}
