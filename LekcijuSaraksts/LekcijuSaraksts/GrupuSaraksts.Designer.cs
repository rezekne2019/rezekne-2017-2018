﻿namespace LekcijuSaraksts
{
    partial class GrupuSaraksts
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.grupuSarakstsTable = new System.Windows.Forms.DataGridView();
            this.pievienotBtn = new System.Windows.Forms.Button();
            this.redigetBtn = new System.Windows.Forms.Button();
            this.dzestBtn = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.grupuSarakstsTable)).BeginInit();
            this.SuspendLayout();
            // 
            // grupuSarakstsTable
            // 
            this.grupuSarakstsTable.AllowUserToAddRows = false;
            this.grupuSarakstsTable.AllowUserToDeleteRows = false;
            this.grupuSarakstsTable.AllowUserToOrderColumns = true;
            this.grupuSarakstsTable.AllowUserToResizeRows = false;
            this.grupuSarakstsTable.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grupuSarakstsTable.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.grupuSarakstsTable.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.grupuSarakstsTable.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.grupuSarakstsTable.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grupuSarakstsTable.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.grupuSarakstsTable.Location = new System.Drawing.Point(12, 12);
            this.grupuSarakstsTable.MultiSelect = false;
            this.grupuSarakstsTable.Name = "grupuSarakstsTable";
            this.grupuSarakstsTable.ReadOnly = true;
            this.grupuSarakstsTable.RowHeadersVisible = false;
            this.grupuSarakstsTable.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.grupuSarakstsTable.Size = new System.Drawing.Size(534, 258);
            this.grupuSarakstsTable.TabIndex = 1;
            // 
            // pievienotBtn
            // 
            this.pievienotBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.pievienotBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(38)))), ((int)(((byte)(134)))), ((int)(((byte)(55)))));
            this.pievienotBtn.FlatAppearance.BorderSize = 0;
            this.pievienotBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.pievienotBtn.ForeColor = System.Drawing.Color.White;
            this.pievienotBtn.Location = new System.Drawing.Point(12, 277);
            this.pievienotBtn.Name = "pievienotBtn";
            this.pievienotBtn.Size = new System.Drawing.Size(75, 23);
            this.pievienotBtn.TabIndex = 2;
            this.pievienotBtn.Text = "PIEVIENOT";
            this.pievienotBtn.UseVisualStyleBackColor = false;
            this.pievienotBtn.Click += new System.EventHandler(this.pievienotBtn_Click);
            // 
            // redigetBtn
            // 
            this.redigetBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.redigetBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(16)))), ((int)(((byte)(125)))), ((int)(((byte)(190)))));
            this.redigetBtn.FlatAppearance.BorderSize = 0;
            this.redigetBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.redigetBtn.ForeColor = System.Drawing.Color.White;
            this.redigetBtn.Location = new System.Drawing.Point(93, 277);
            this.redigetBtn.Name = "redigetBtn";
            this.redigetBtn.Size = new System.Drawing.Size(75, 23);
            this.redigetBtn.TabIndex = 3;
            this.redigetBtn.Text = "REDIĢĒT";
            this.redigetBtn.UseVisualStyleBackColor = false;
            this.redigetBtn.Click += new System.EventHandler(this.redigetBtn_Click);
            // 
            // dzestBtn
            // 
            this.dzestBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.dzestBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(149)))), ((int)(((byte)(0)))), ((int)(((byte)(4)))));
            this.dzestBtn.FlatAppearance.BorderSize = 0;
            this.dzestBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.dzestBtn.ForeColor = System.Drawing.Color.White;
            this.dzestBtn.Location = new System.Drawing.Point(174, 277);
            this.dzestBtn.Name = "dzestBtn";
            this.dzestBtn.Size = new System.Drawing.Size(75, 23);
            this.dzestBtn.TabIndex = 4;
            this.dzestBtn.Text = "DZĒST";
            this.dzestBtn.UseVisualStyleBackColor = false;
            this.dzestBtn.Click += new System.EventHandler(this.dzestBtn_Click);
            // 
            // GrupuSaraksts
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(559, 312);
            this.Controls.Add(this.dzestBtn);
            this.Controls.Add(this.redigetBtn);
            this.Controls.Add(this.pievienotBtn);
            this.Controls.Add(this.grupuSarakstsTable);
            this.Name = "GrupuSaraksts";
            this.Text = "Grupu saraksts";
            ((System.ComponentModel.ISupportInitialize)(this.grupuSarakstsTable)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.DataGridView grupuSarakstsTable;
        private System.Windows.Forms.Button pievienotBtn;
        private System.Windows.Forms.Button redigetBtn;
        private System.Windows.Forms.Button dzestBtn;
    }
}