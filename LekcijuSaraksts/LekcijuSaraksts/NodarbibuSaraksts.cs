﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SarakstaEntites;

namespace LekcijuSaraksts
{
    public partial class NodarbibuSaraksts : Form
    {
        private SarakstaEntitesContainer database;
        public NodarbibuSaraksts(SarakstaEntitesContainer database)
        {
            this.database = database;
            InitializeComponent();
            RefreshTable();
        }
        private void RefreshTable()
        {
            var query = database.NodarbibaSet.Select(x => new
            {
                Id = x.Id,
                diena = x.Diena.Nosaukums,
                laiks = x.Laiks.Nodarbibas_laiks,
                telpa = x.Telpa.Telpas_nr,
                eka = x.Telpa.Telpas_eka,
                docetajs = x.Docetajs.Vards + " " + x.Docetajs.Uzvards,
                kurss = x.LekcijuKurss.Nosaukums,
                grupa = x.GrupaNodarbiba.Where(y => y.Nodarbiba.Id == x.Id)
                        });
            NodarbibuTable.DataSource = query.ToList();

        }

        private Nodarbiba GetSelectedNodarbiba()
        {
            if (NodarbibuTable.SelectedCells.Count <= 0)
                return null;

            int idx = NodarbibuTable.SelectedCells[0].RowIndex;

            DataGridViewRow row = NodarbibuTable.Rows[idx];

            int a = Convert.ToInt32(row.Cells["Id"].Value);
            var entity = database.NodarbibaSet.SingleOrDefault(x => x.Id == a);
            return entity;
        }

        private void pievienotBtn_Click(object sender, EventArgs e)
        {
            var dialog = new NodarbibuDialogs(database);
            dialog.FormClosed += delegate { RefreshTable(); };
            dialog.ShowDialog();
        }
        private void dzestBtn_Click(object sender, EventArgs e)
        {
            var nodarbiba = GetSelectedNodarbiba();
            if (nodarbiba == null)
                return;
            var choice = MessageBox.Show(
                $"Dzēst Nodarbību?",
                "Dzēst Nodarbību?",
                MessageBoxButtons.YesNo,
                MessageBoxIcon.Warning);

            if (choice == DialogResult.Yes)
            {
                database.NodarbibaSet.Remove(nodarbiba);
                database.GrupaNodarbibaSet.RemoveRange(database.GrupaNodarbibaSet.Where(x => x.Nodarbiba.Id == nodarbiba.Id));
                database.SaveChanges();
                RefreshTable();
            }
        }
    }
}
