﻿namespace LekcijuSaraksts
{
     partial class LekcijuSaraksts
     {
          /// <summary>
          /// Required designer variable.
          /// </summary>
          private System.ComponentModel.IContainer components = null;

          /// <summary>
          /// Clean up any resources being used.
          /// </summary>
          /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
          protected override void Dispose(bool disposing)
          {
               if (disposing && (components != null))
               {
                    components.Dispose();
               }
               base.Dispose(disposing);
          }

          #region Windows Form Designer generated code

          /// <summary>
          /// Required method for Designer support - do not modify
          /// the contents of this method with the code editor.
          /// </summary>
          private void InitializeComponent()
          {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(LekcijuSaraksts));
            this.menuStrip = new System.Windows.Forms.MenuStrip();
            this.viewMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.studijuKursiToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.grupasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.docētājiToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.nodarbībasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.lekcijuKursiToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStrip = new System.Windows.Forms.ToolStrip();
            this.newToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.openToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.saveToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.printToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.printPreviewToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.helpToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.statusStrip = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolTip = new System.Windows.Forms.ToolTip(this.components);
            this.dienaComboBox = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.semestrisComboBox = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.programmaComboBox = new System.Windows.Forms.ComboBox();
            this.raditBtn = new System.Windows.Forms.Button();
            this.lekcijuSarakstsTable = new System.Windows.Forms.DataGridView();
            this.menuStrip.SuspendLayout();
            this.toolStrip.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lekcijuSarakstsTable)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrip
            // 
            this.menuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.viewMenu,
            this.helpMenu});
            this.menuStrip.Location = new System.Drawing.Point(0, 0);
            this.menuStrip.Name = "menuStrip";
            this.menuStrip.Size = new System.Drawing.Size(570, 24);
            this.menuStrip.TabIndex = 0;
            this.menuStrip.Text = "MenuStrip";
            // 
            // viewMenu
            // 
            this.viewMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.studijuKursiToolStripMenuItem,
            this.grupasToolStripMenuItem,
            this.docētājiToolStripMenuItem,
            this.nodarbībasToolStripMenuItem,
            this.lekcijuKursiToolStripMenuItem});
            this.viewMenu.Name = "viewMenu";
            this.viewMenu.Size = new System.Drawing.Size(77, 20);
            this.viewMenu.Text = "&Kategorijas";
            // 
            // studijuKursiToolStripMenuItem
            // 
            this.studijuKursiToolStripMenuItem.Name = "studijuKursiToolStripMenuItem";
            this.studijuKursiToolStripMenuItem.Size = new System.Drawing.Size(139, 22);
            this.studijuKursiToolStripMenuItem.Text = "Studiju kursi";
            this.studijuKursiToolStripMenuItem.Click += new System.EventHandler(this.studijuProgrammasToolStripMenuItem_Click);
            // 
            // grupasToolStripMenuItem
            // 
            this.grupasToolStripMenuItem.Name = "grupasToolStripMenuItem";
            this.grupasToolStripMenuItem.Size = new System.Drawing.Size(139, 22);
            this.grupasToolStripMenuItem.Text = "Grupas";
            this.grupasToolStripMenuItem.Click += new System.EventHandler(this.grupasToolStripMenuItem_Click);
            // 
            // docētājiToolStripMenuItem
            // 
            this.docētājiToolStripMenuItem.Name = "docētājiToolStripMenuItem";
            this.docētājiToolStripMenuItem.Size = new System.Drawing.Size(139, 22);
            this.docētājiToolStripMenuItem.Text = "Docētāji";
            this.docētājiToolStripMenuItem.Click += new System.EventHandler(this.docētājiToolStripMenuItem_Click);
            // 
            // nodarbībasToolStripMenuItem
            // 
            this.nodarbībasToolStripMenuItem.Name = "nodarbībasToolStripMenuItem";
            this.nodarbībasToolStripMenuItem.Size = new System.Drawing.Size(139, 22);
            this.nodarbībasToolStripMenuItem.Text = "Nodarbības";
            this.nodarbībasToolStripMenuItem.Click += new System.EventHandler(this.nodarbībasToolStripMenuItem_Click);
            // 
            // lekcijuKursiToolStripMenuItem
            // 
            this.lekcijuKursiToolStripMenuItem.Name = "lekcijuKursiToolStripMenuItem";
            this.lekcijuKursiToolStripMenuItem.Size = new System.Drawing.Size(139, 22);
            this.lekcijuKursiToolStripMenuItem.Text = "Lekciju kursi";
            this.lekcijuKursiToolStripMenuItem.Click += new System.EventHandler(this.lekcijuKursiToolStripMenuItem_Click);
            // 
            // helpMenu
            // 
            this.helpMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.aboutToolStripMenuItem});
            this.helpMenu.Name = "helpMenu";
            this.helpMenu.Size = new System.Drawing.Size(66, 20);
            this.helpMenu.Text = "&Palīdzība";
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(167, 22);
            this.aboutToolStripMenuItem.Text = "&Par programmu...";
            this.aboutToolStripMenuItem.Click += new System.EventHandler(this.aboutToolStripMenuItem_Click_1);
            // 
            // toolStrip
            // 
            this.toolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newToolStripButton,
            this.openToolStripButton,
            this.saveToolStripButton,
            this.toolStripSeparator1,
            this.printToolStripButton,
            this.printPreviewToolStripButton,
            this.toolStripSeparator2,
            this.helpToolStripButton});
            this.toolStrip.Location = new System.Drawing.Point(0, 24);
            this.toolStrip.Name = "toolStrip";
            this.toolStrip.Size = new System.Drawing.Size(632, 25);
            this.toolStrip.TabIndex = 1;
            this.toolStrip.Text = "ToolStrip";
            this.toolStrip.Visible = false;
            // 
            // newToolStripButton
            // 
            this.newToolStripButton.Name = "newToolStripButton";
            this.newToolStripButton.Size = new System.Drawing.Size(23, 22);
            // 
            // openToolStripButton
            // 
            this.openToolStripButton.Name = "openToolStripButton";
            this.openToolStripButton.Size = new System.Drawing.Size(23, 22);
            // 
            // saveToolStripButton
            // 
            this.saveToolStripButton.Name = "saveToolStripButton";
            this.saveToolStripButton.Size = new System.Drawing.Size(23, 22);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // printToolStripButton
            // 
            this.printToolStripButton.Name = "printToolStripButton";
            this.printToolStripButton.Size = new System.Drawing.Size(23, 22);
            // 
            // printPreviewToolStripButton
            // 
            this.printPreviewToolStripButton.Name = "printPreviewToolStripButton";
            this.printPreviewToolStripButton.Size = new System.Drawing.Size(23, 22);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // helpToolStripButton
            // 
            this.helpToolStripButton.Name = "helpToolStripButton";
            this.helpToolStripButton.Size = new System.Drawing.Size(23, 22);
            // 
            // statusStrip
            // 
            this.statusStrip.Location = new System.Drawing.Point(0, 431);
            this.statusStrip.Name = "statusStrip";
            this.statusStrip.Size = new System.Drawing.Size(570, 22);
            this.statusStrip.TabIndex = 14;
            // 
            // toolStripStatusLabel
            // 
            this.toolStripStatusLabel.Name = "toolStripStatusLabel";
            this.toolStripStatusLabel.Size = new System.Drawing.Size(23, 23);
            // 
            // dienaComboBox
            // 
            this.dienaComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.dienaComboBox.FormattingEnabled = true;
            this.dienaComboBox.Location = new System.Drawing.Point(12, 49);
            this.dienaComboBox.Name = "dienaComboBox";
            this.dienaComboBox.Size = new System.Drawing.Size(121, 21);
            this.dienaComboBox.TabIndex = 5;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Location = new System.Drawing.Point(12, 33);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = "Diena";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Location = new System.Drawing.Point(152, 33);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(52, 13);
            this.label2.TabIndex = 7;
            this.label2.Text = "Semestris";
            // 
            // semestrisComboBox
            // 
            this.semestrisComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.semestrisComboBox.FormattingEnabled = true;
            this.semestrisComboBox.Location = new System.Drawing.Point(155, 49);
            this.semestrisComboBox.Name = "semestrisComboBox";
            this.semestrisComboBox.Size = new System.Drawing.Size(70, 21);
            this.semestrisComboBox.TabIndex = 8;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Location = new System.Drawing.Point(242, 33);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(60, 13);
            this.label3.TabIndex = 9;
            this.label3.Text = "Programma";
            // 
            // programmaComboBox
            // 
            this.programmaComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.programmaComboBox.FormattingEnabled = true;
            this.programmaComboBox.Location = new System.Drawing.Point(245, 49);
            this.programmaComboBox.Name = "programmaComboBox";
            this.programmaComboBox.Size = new System.Drawing.Size(217, 21);
            this.programmaComboBox.TabIndex = 10;
            // 
            // raditBtn
            // 
            this.raditBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.raditBtn.Location = new System.Drawing.Point(483, 47);
            this.raditBtn.Name = "raditBtn";
            this.raditBtn.Size = new System.Drawing.Size(75, 23);
            this.raditBtn.TabIndex = 12;
            this.raditBtn.Text = "Parādīt";
            this.raditBtn.UseVisualStyleBackColor = true;
            this.raditBtn.Click += new System.EventHandler(this.raditBtn_Click);
            // 
            // lekcijuSarakstsTable
            // 
            this.lekcijuSarakstsTable.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lekcijuSarakstsTable.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.lekcijuSarakstsTable.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.lekcijuSarakstsTable.Location = new System.Drawing.Point(15, 90);
            this.lekcijuSarakstsTable.Name = "lekcijuSarakstsTable";
            this.lekcijuSarakstsTable.Size = new System.Drawing.Size(543, 326);
            this.lekcijuSarakstsTable.TabIndex = 13;
            // 
            // LekcijuSaraksts
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(570, 453);
            this.Controls.Add(this.lekcijuSarakstsTable);
            this.Controls.Add(this.raditBtn);
            this.Controls.Add(this.programmaComboBox);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.semestrisComboBox);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dienaComboBox);
            this.Controls.Add(this.statusStrip);
            this.Controls.Add(this.toolStrip);
            this.Controls.Add(this.menuStrip);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.IsMdiContainer = true;
            this.MainMenuStrip = this.menuStrip;
            this.Name = "LekcijuSaraksts";
            this.Text = "LekcijuSaraksts";
            this.menuStrip.ResumeLayout(false);
            this.menuStrip.PerformLayout();
            this.toolStrip.ResumeLayout(false);
            this.toolStrip.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lekcijuSarakstsTable)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

          }
          #endregion


          private System.Windows.Forms.MenuStrip menuStrip;
          private System.Windows.Forms.ToolStrip toolStrip;
          private System.Windows.Forms.StatusStrip statusStrip;
          private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
          private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
          private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel;
          private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
          private System.Windows.Forms.ToolStripMenuItem viewMenu;
          private System.Windows.Forms.ToolStripMenuItem helpMenu;
          private System.Windows.Forms.ToolStripButton newToolStripButton;
          private System.Windows.Forms.ToolStripButton openToolStripButton;
          private System.Windows.Forms.ToolStripButton saveToolStripButton;
          private System.Windows.Forms.ToolStripButton printToolStripButton;
          private System.Windows.Forms.ToolStripButton printPreviewToolStripButton;
          private System.Windows.Forms.ToolStripButton helpToolStripButton;
          private System.Windows.Forms.ToolTip toolTip;
          private System.Windows.Forms.ToolStripMenuItem studijuKursiToolStripMenuItem;
          private System.Windows.Forms.ToolStripMenuItem grupasToolStripMenuItem;
          private System.Windows.Forms.ToolStripMenuItem docētājiToolStripMenuItem;
          private System.Windows.Forms.ToolStripMenuItem nodarbībasToolStripMenuItem;
          private System.Windows.Forms.ToolStripMenuItem lekcijuKursiToolStripMenuItem;
        private System.Windows.Forms.ComboBox dienaComboBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox semestrisComboBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox programmaComboBox;
        private System.Windows.Forms.Button raditBtn;
        private System.Windows.Forms.DataGridView lekcijuSarakstsTable;
    }
}



