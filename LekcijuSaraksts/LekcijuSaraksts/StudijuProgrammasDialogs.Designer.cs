﻿namespace LekcijuSaraksts
{
     partial class StudijuProgrammasDialogs
     {
          /// <summary>
          /// Required designer variable.
          /// </summary>
          private System.ComponentModel.IContainer components = null;

          /// <summary>
          /// Clean up any resources being used.
          /// </summary>
          /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
          protected override void Dispose(bool disposing)
          {
               if (disposing && (components != null))
               {
                    components.Dispose();
               }
               base.Dispose(disposing);
          }

          #region Windows Form Designer generated code

          /// <summary>
          /// Required method for Designer support - do not modify
          /// the contents of this method with the code editor.
          /// </summary>
          private void InitializeComponent()
          {
               this.cancelBtn = new System.Windows.Forms.Button();
               this.saveBtn = new System.Windows.Forms.Button();
               this.label1 = new System.Windows.Forms.Label();
               this.nameTextBox = new System.Windows.Forms.TextBox();
               this.label2 = new System.Windows.Forms.Label();
               this.sifrsTextBox = new System.Windows.Forms.TextBox();
               this.SuspendLayout();
               // 
               // cancelBtn
               // 
               this.cancelBtn.BackColor = System.Drawing.Color.Gray;
               this.cancelBtn.FlatAppearance.BorderSize = 0;
               this.cancelBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
               this.cancelBtn.ForeColor = System.Drawing.Color.White;
               this.cancelBtn.Location = new System.Drawing.Point(99, 135);
               this.cancelBtn.Name = "cancelBtn";
               this.cancelBtn.Size = new System.Drawing.Size(75, 23);
               this.cancelBtn.TabIndex = 15;
               this.cancelBtn.Text = "ATCELT";
               this.cancelBtn.UseVisualStyleBackColor = false;
               this.cancelBtn.Click += new System.EventHandler(this.cancelBtn_Click);
               // 
               // saveBtn
               // 
               this.saveBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(16)))), ((int)(((byte)(125)))), ((int)(((byte)(190)))));
               this.saveBtn.FlatAppearance.BorderSize = 0;
               this.saveBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
               this.saveBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
               this.saveBtn.ForeColor = System.Drawing.Color.White;
               this.saveBtn.Location = new System.Drawing.Point(18, 135);
               this.saveBtn.Name = "saveBtn";
               this.saveBtn.Size = new System.Drawing.Size(75, 23);
               this.saveBtn.TabIndex = 14;
               this.saveBtn.Text = "SAGLABĀT";
               this.saveBtn.UseVisualStyleBackColor = false;
               this.saveBtn.Click += new System.EventHandler(this.saveBtn_Click);
               // 
               // label1
               // 
               this.label1.AutoSize = true;
               this.label1.Location = new System.Drawing.Point(15, 17);
               this.label1.Name = "label1";
               this.label1.Size = new System.Drawing.Size(63, 13);
               this.label1.TabIndex = 9;
               this.label1.Text = "Nosaukums";
               // 
               // nameTextBox
               // 
               this.nameTextBox.Location = new System.Drawing.Point(18, 33);
               this.nameTextBox.Name = "nameTextBox";
               this.nameTextBox.Size = new System.Drawing.Size(320, 20);
               this.nameTextBox.TabIndex = 8;
               // 
               // label2
               // 
               this.label2.AutoSize = true;
               this.label2.Location = new System.Drawing.Point(15, 65);
               this.label2.Name = "label2";
               this.label2.Size = new System.Drawing.Size(27, 13);
               this.label2.TabIndex = 11;
               this.label2.Text = "Šifrs";
               // 
               // sifrsTextBox
               // 
               this.sifrsTextBox.Location = new System.Drawing.Point(18, 81);
               this.sifrsTextBox.Name = "sifrsTextBox";
               this.sifrsTextBox.Size = new System.Drawing.Size(320, 20);
               this.sifrsTextBox.TabIndex = 10;
               // 
               // StudijuProgrammasDialogs
               // 
               this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
               this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
               this.ClientSize = new System.Drawing.Size(352, 166);
               this.Controls.Add(this.cancelBtn);
               this.Controls.Add(this.saveBtn);
               this.Controls.Add(this.label1);
               this.Controls.Add(this.nameTextBox);
               this.Controls.Add(this.label2);
               this.Controls.Add(this.sifrsTextBox);
               this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
               this.MaximizeBox = false;
               this.MinimizeBox = false;
               this.Name = "StudijuProgrammasDialogs";
               this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
               this.Text = "Studiju programma";
               this.ResumeLayout(false);
               this.PerformLayout();

          }

          #endregion

          private System.Windows.Forms.Button cancelBtn;
          private System.Windows.Forms.Button saveBtn;
          private System.Windows.Forms.Label label1;
          private System.Windows.Forms.TextBox nameTextBox;
          private System.Windows.Forms.Label label2;
          private System.Windows.Forms.TextBox sifrsTextBox;
     }
}